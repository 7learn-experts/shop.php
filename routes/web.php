<?php
return [
	'/' => [
		'method' => 'get|post',
		'target'=>'HomeController@index',
		'middleware'=>'SecurityMiddleware',
	],
	'/contact' => [
		'method' => 'get',
		'target'=>'HomeController@contact',
	],
	'/user/orders' => [
		'method' => 'get',
		'target'=>'UserController@orders',
	],
	'/user/register' => [
		'method' => 'post|get',
		'target'=>'UserController@register',
		'middleware'=>'SecurityMiddleware'
	],
	'/product/item' => [
		'method' => 'get',
		'target'=>'ProductController@item',
	],

];


<?php
namespace App\Controllers;

use App\Core\Request;
use App\Repositories\ProductRepo;
use App\Services\View\View;

class ProductController{
	private $productRepo;
//	private $orderModel;

	public function __construct() {
		$this->productRepo = new ProductRepo();
//		$this->$orderModel = new OrderModel();
	}

	public function item(Request $request) {

		$item = $this->productRepo->find($request->param('id'));
		$data = [
			'product'=> $item
		];
		View::load('product.single',$data);
	}

}
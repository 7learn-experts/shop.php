<?php
namespace App\Repositories;

abstract class BaseRepo {
	protected static $model ;

	public function insert($data) {
		return static::$model::insert($data);
	}

	public function find($id) {
		return static::$model::find($id)->get();
	}

	public function all() {
		return static::$model::all();
	}

/*	public function update($data,$id) {
		return $this->model::update($data,$id);
	}

	public function delete($id) {
		return $this->model::delete($id);

	}*/
}
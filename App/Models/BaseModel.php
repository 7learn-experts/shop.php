<?php
namespace App\Models;

use App\Services\Database\DbConnection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model {
	protected $table ;
	protected $primaryKey ;
	public $timestamps = false ;




}
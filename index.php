<?php

error_reporting(E_ALL);
ini_set('display_errors',1);

include_once "vendor/autoload.php";
include_once "bootstrap/Constants.php";
include_once "bootstrap/init.php";

include_once "App/Services/Router/Router.php";

\App\Services\Router\Router::register();

